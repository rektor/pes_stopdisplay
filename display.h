/*
 * Display module header file.
 *
 */


#ifndef DISPLAY_H
#define DISPLAY_H

#include "global.h"

#define WIDTH 320

void DrawAnyLine(int x0,int y0,int x1,int y1);
void DrawFilledCircle(int x0, int y0, int radius);
void Create_Bus_button(int x, int y);

#endif
