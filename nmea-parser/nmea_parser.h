#ifndef __nmea_parser_h__
#define __nmea_parser_h__

#define NUM_TYPES 8 //excluding t_end
#define NUM_MSGS 6 //excluding NMEA_ERROR

/* Types of fields in the messages */
enum Types {
  t_id,
  t_char,
  t_double,
  t_int,
  t_time,
  t_date,
  t_int4,
  t_int12,
  t_end
};

/* Types of messges, serves as indexes for lookup tables aswell */
typedef enum {GPGGA, GPGLL, GPGSA, GPGSV, GPRMC, GPVTG, NMEA_ERROR } MSG_ID;





struct Time {
  int hours;
  int minutes;
  double seconds;
};

struct Date {
  int year;
  int month;
  int day;
};

struct s_GPGGA {
  MSG_ID id;
  struct Time time;
  double latitude;
  char ns;
  double longitude;
  char ew;
  int pos_indicator;
  int sats_used;
  double hdop;
  double msl_altitude;
  char alt_unit;
  double geoidal_separation;
  char geo_unit;
  //double age_of_diff_corr;
  //double diff_ref_station_id
};

struct s_GPGLL {
  MSG_ID id;
  double latitude;
  char ns;
  double longitude;
  char ew;
  struct Time time;
  char status;
  char mode;
};

struct s_GPGSA {
  MSG_ID id;
  char mode1;
  int mode2;
  int sats_used[12];
  double pdop;
  double hdop;
  double vdop;
};

struct s_GPGSV {
  MSG_ID id;
  int num_msgs;
  int msg_num;
  int sats_in_view;
  int sv1[4];  //sv prn #, elevation, azimuth, snr
  int sv2[4];
  int sv3[4];
  int sv4[4];
};

struct s_GPRMC {
  MSG_ID id;
  struct Time time;
  char status;
  double latitude;
  char ns;
  double longitude;
  char ew;
  double speed;
  double course;
  struct Date date;
  int dummy1; // module does not support these fields
  int dummy2;
  char mode;
};

struct s_GPVTG {
  MSG_ID id;
  double course1;
  char reference1;
  double course2;
  char reference2;
  double speed1;
  char unit1;
  double speed2;
  char unit2;
  char mode;
};
  
struct messages {
  struct s_GPGGA gpgga;
  struct s_GPGLL gpgll;
  struct s_GPGSA gpgsa;
  struct s_GPGSV gpgsv;
  struct s_GPRMC gprmc;
  struct s_GPVTG gpvtg;
};


int parse_id(char **msg, void *result);
int parse_double(char **msg, void *result);
int parse_time(char **msg, void *result);
int parse_int(char **msg, void *result);
int parse_date(char **msg, void *result);
int parse_char(char **msg, void *result);
int parse_int4(char **msg, void *result);
int parse_int12(char **msg, void *result);
int parse_two(char *msg);
int checksum(char *s);
MSG_ID parser(char *msg, struct messages *ret);


#endif
